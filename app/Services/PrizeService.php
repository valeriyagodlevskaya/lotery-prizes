<?php

namespace App\Services;

use App\Interfaces\PrizeInterface;
use App\Models\PrizeBonus;
use App\Models\PrizeItem;
use App\Models\PrizeMoney;

class PrizeService
{
    const TYPE_PRIZES = [
        PrizeMoney::class,
        PrizeBonus::class,
        PrizeItem::class
    ];

    /**
     * @return PrizeInterface
     * @throws \Exception
     */
    public static function getRandomPrize(): PrizeInterface
    {
        $model = self::TYPE_PRIZES[random_int(0, count(self::TYPE_PRIZES) - 1)];

        return new $model;
    }

    /**
     * @return mixed
     */
    public function getWin(): array
    {
        return self::getRandomPrize()->getWin();
    }

    /**
     * @param string $type
     * @return string
     */
    public static function getClassForType(string $type): string
    {
        $list = [
            PrizeMoney::TYPE => PrizeMoney::class,
            PrizeBonus::TYPE => PrizeBonus::class,
            PrizeItem::TYPE => PrizeItem::class
        ];

        return $list[$type];
    }

    /**
     * @param string $type
     * @param $value
     * @param int $userId
     * @throws \Exception
     */
    public function savePrize(string $type, $value, int $userId): void
    {
        $class = self::getClassForType($type);
        $model = new $class;
        $res = $model->savePrize($userId, $value);
        if (!$res->id){
            throw new \Exception('Wrong! Dont save prize.');
        }

    }

    /**
     * @param string $type
     * @param $value
     * @param int $userId
     * @throws \Exception
     */
    public function convertInBonus(string $type, $value, int $userId): void
    {
        $class = self::getClassForType($type);
        $model = new $class;
        $newVal = $model->convert($value);
        $model->savePrize($userId, $newVal);
        if (!$model){
            throw new \Exception('Wrong! Not convert and save prize.');
        }
    }

}
