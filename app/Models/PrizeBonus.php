<?php

namespace App\Models;

use App\Interfaces\PrizeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrizeBonus extends Model implements PrizeInterface
{
    use HasFactory;

    const TYPE = 'bonus';
    const MIN_SUMM = 10;
    const MAX_SUMM = 1000;
    const RATE = 3;

    protected $table = 'prize_bonus';

    protected $fillable = [
        'user_id',
        'amount'
    ];

    /**
     * @return mixed
     */
    public function getWin()
    {
        return ['type' => self::TYPE, 'value' => random_int(self::MIN_SUMM, self::MAX_SUMM)];
    }

    /**
     * @param int $userId
     * @param string $value
     * @return mixed
     */
    public function savePrize(int $userId, string $value)
    {
        return self::query()->create(['user_id' => $userId, 'amount' => $value]);
    }

    /**
     * @param int $value
     * @return float|int
     */
    public function convert(int $value)
    {
        return $value/self::RATE;
    }
}
