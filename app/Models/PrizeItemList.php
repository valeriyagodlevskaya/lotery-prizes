<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrizeItemList extends Model
{
    use HasFactory;

    const MIN_COUNT = 0;

    protected $fillable = [
        'title',
        'status',
        'count'
    ];

    public function prizeItem()
    {
        return $this->belongsTo('App\Model\PrizeItem');
    }

    public static function getItemsList()
    {
        return self::query()->where('count', '!=', self::MIN_COUNT)->get(['id', 'title'])->toArray();
    }

}
