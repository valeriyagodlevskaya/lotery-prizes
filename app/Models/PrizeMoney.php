<?php

namespace App\Models;

use App\Interfaces\PrizeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrizeMoney extends Model implements PrizeInterface
{
    use HasFactory;

    const TYPE = 'money';
    const MIN_SUMM = 10;
    const MAX_SUMM = 1000;
    const STATUS_NOT_SENT_TO_BANK = 0;
    const STATUS_SENT_TO_BANK = 1;

    protected $fillable = [
        'user_id',
        'amount',
        'status'
    ];

    /**
     * @return mixed
     */
    public function getWin()
    {
        return ['type' => self::TYPE, 'value' => $this->randomCash(), 'bank' => $this->getBank()];
    }

    /**
     * @param int $userId
     * @param string $value
     * @return mixed
     */
    public function savePrize(int $userId, string $value)
    {
        Setting::changeBank($value);
        return self::query()->create(['user_id' => $userId, 'amount' => $value, 'status' => self::STATUS_NOT_SENT_TO_BANK]);
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function randomCash()
    {
        $value = random_int(self::MIN_SUMM, self::MAX_SUMM);
        if ($value > $this->getBank()){
            $value = random_int(self::MIN_SUMM, $this->getBank());
        }
        return $value;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return Setting::getPrizeBank();
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
