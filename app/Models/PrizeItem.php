<?php

namespace App\Models;

use App\Interfaces\PrizeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrizeItem extends Model implements PrizeInterface
{
    use HasFactory;

    const TYPE = 'item';
    const STATUS_NOT_SENT = 0;
    const STATUS_SENT = 1;

    protected $fillable = [
        'user_id',
        'prize_item_list_id',
        'status'
    ];

    /**
     * @return mixed
     */
    public function getWin(): array
    {
        return $this->getPrizeItem();
    }

    /**
     * @return mixed
     */
    protected function prizeItemList()
    {
        return $this->hasMany('App\Models\PrizeItemList');
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getPrizeItem(): array
    {
        $item = PrizeItemList::getItemsList();
        $prizeItem = array_slice($item, random_int(0, count($item)-1), 1)[0];
        return [
            'type' => self::TYPE,
            'id' => $prizeItem['id'],
            'value' => $prizeItem['title']
        ];
    }

    /**
     * @param int $userId
     * @param string $value
     * @return mixed
     */
    public function savePrize(int $userId, string $value)
    {
        $this->updateCountItem($value);

        return self::query()->create(['user_id' => $userId, 'prize_item_list_id' => $value, 'status' => self::STATUS_NOT_SENT]);
    }

    public function updateCountItem(int $id): void
    {
        $item = PrizeItemList::find($id);
        $item->count--;
        $item->save();
    }
}
