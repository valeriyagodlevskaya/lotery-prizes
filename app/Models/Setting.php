<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    const KEY_BANK = 'prize_bank';

    protected $fillable = [
        'key',
        'value'
    ];

    public $timestamps = false;

    /**
     * @return mixed
     */
    public static function getPrizeBank()
    {
        return self::query()->where('key', '=', self::KEY_BANK)->get()->toArray()[0]['value'];
    }

    /**
     * @param int $amount
     * @return int
     */
    public static function changeBank(int $amount)
    {
        $reCount = self::getPrizeBank() - $amount;
        return self::query()->where('key', '=', self::KEY_BANK)->update(['value' => $reCount]);

    }

}
