<?php

namespace App\Console\Commands;

use App\Models\PrizeMoney;
use App\Models\User;
use App\Services\Api\BankApiService;
use Illuminate\Console\Command;

class SendBank extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendBank';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command send cash users in bank';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::with('prizeMoney')->get();
        BankApiService::send($users);
    }
}
