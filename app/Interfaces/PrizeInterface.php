<?php

namespace App\Interfaces;

interface PrizeInterface
{
    public function getWin();

    public function savePrize(int $userId, string $value);
}
