<?php

namespace App\Http\Controllers;

use App\Services\PrizeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrizeController extends Controller
{
    protected $prizeService;

    /**
     * Create a new controller instance.
     *
     * @param PrizeService $prizeService
     */
    public function __construct(PrizeService $prizeService)
    {
        $this->middleware('auth');
        $this->prizeService = $prizeService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function winPrize()
    {
        return view('home', ['prize' => $this->prizeService->getWin()]);

    }

    public function cancelPrize()
    {
        return view('home');
    }

    public function takePrize(Request $request)
    {
        $this->prizeService->savePrize($request->input('type'), $request->input('prize'), Auth::id());

        return view('home');
    }

    public function convertInBonus(Request $request)
    {
        $this->prizeService->convertInBonus($request->input('type'), $request->input('prize'), Auth::id());

        return view('home');
    }
}
