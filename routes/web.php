<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\PrizeController::class, 'index'])->name('home');

Route::post('/winPrize', [App\Http\Controllers\PrizeController::class, 'winPrize'])->name('winPrize');

Route::get('/cancelPrize', [App\Http\Controllers\PrizeController::class, 'cancelPrize'])->name('cancelPrize');

Route::post('/takePrize', [App\Http\Controllers\PrizeController::class, 'takePrize'])->name('takePrize');

Route::post('/convertInBonus', [App\Http\Controllers\PrizeController::class, 'convertInBonus'])->name('convertInBonus');
