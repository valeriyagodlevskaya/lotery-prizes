@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <form class="form-group" method="POST" action="{{ route('winPrize') }}">
                       @csrf
                       <button class="form-control">Get gift</button>
                   </form>
                    @isset($prize)
                        <form class="form-group" method="POST">
                            @csrf
                            <label class="text-danger">You Win!!</label>
                            <h3>{{ $prize['value'] }} <b>@php if($prize['type'] == 'money') echo '$'; elseif($prize['type'] == 'bonus') echo 'points'; else echo ''; @endphp</b> </h3>
                            <input class="text-hide" name="prize" value="{{ $prize['id'] ??  $prize['value']}}">
                            <input class="text-hide" name="type" value="{{ $prize['type'] }}">
                            <div class="col-md-12 form-group">
                                <button formaction="{{ route('takePrize') }}" class="btn btn-dark col-md-4">Take prize</button>
                                @if($prize['type'] === 'money')
                                    <button formaction="{{ route('convertInBonus') }}" class=" btn btn-primary col-md-4">Convert to point</button>
                                @endif
                                <a href="{{ route('cancelPrize') }}" class="btn btn-danger col-md-3">Cancel prize</a>
                            </div>
                        </form>
                    @endisset

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
