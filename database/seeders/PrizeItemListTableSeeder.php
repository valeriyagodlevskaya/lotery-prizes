<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrizeItemListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prize_item_lists')->insert([
            ['title' => 'AirPods', 'count' => 100, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['title' => 'Notebook', 'count' => 100, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['title' => 'Blender', 'count' => 100, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['title' => 'Car', 'count' => 100, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['title' => 'Phone','count' => 100, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
